﻿namespace MajorProject2App
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.outputListBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.expButton = new System.Windows.Forms.Button();
            this.divButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.minusButton = new System.Windows.Forms.Button();
            this.oneDivXButton = new System.Windows.Forms.Button();
            this.factButton = new System.Windows.Forms.Button();
            this.multTableButton = new System.Windows.Forms.Button();
            this.stDivButton = new System.Windows.Forms.Button();
            this.primeButton = new System.Windows.Forms.Button();
            this.sumValuesButton = new System.Windows.Forms.Button();
            this.maxButton = new System.Windows.Forms.Button();
            this.sosButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.yeetCheckBox = new System.Windows.Forms.CheckBox();
            this.clearEntryButton = new System.Windows.Forms.Button();
            this.zeroButton = new System.Windows.Forms.Button();
            this.lilClearButton = new System.Windows.Forms.Button();
            this.threeButton = new System.Windows.Forms.Button();
            this.twoButton = new System.Windows.Forms.Button();
            this.oneButton = new System.Windows.Forms.Button();
            this.sixButton = new System.Windows.Forms.Button();
            this.fiveButton = new System.Windows.Forms.Button();
            this.fourButton = new System.Windows.Forms.Button();
            this.nineButton = new System.Windows.Forms.Button();
            this.eightButton = new System.Windows.Forms.Button();
            this.sevenButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.exitButton = new System.Windows.Forms.Button();
            this.BigClearButton = new System.Windows.Forms.Button();
            this.displayButton = new System.Windows.Forms.Button();
            this.yTextBox = new System.Windows.Forms.TextBox();
            this.xTextBox = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.valueAtButton = new System.Windows.Forms.Button();
            this.avgButton = new System.Windows.Forms.Button();
            this.rangeButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // outputListBox
            // 
            this.outputListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.outputListBox.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outputListBox.FormattingEnabled = true;
            this.outputListBox.ItemHeight = 15;
            this.outputListBox.Location = new System.Drawing.Point(0, 70);
            this.outputListBox.Name = "outputListBox";
            this.outputListBox.Size = new System.Drawing.Size(383, 182);
            this.outputListBox.TabIndex = 2;
            this.outputListBox.SelectedIndexChanged += new System.EventHandler(this.outputListBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(66, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(230, 45);
            this.label1.TabIndex = 1;
            this.label1.Text = "Calculator App";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // expButton
            // 
            this.expButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expButton.Location = new System.Drawing.Point(6, 19);
            this.expButton.Name = "expButton";
            this.expButton.Size = new System.Drawing.Size(122, 50);
            this.expButton.TabIndex = 0;
            this.expButton.Text = "x&^y";
            this.expButton.UseVisualStyleBackColor = true;
            this.expButton.Click += new System.EventHandler(this.expButton_Click);
            this.expButton.MouseEnter += new System.EventHandler(this.expButton_MouseEnter);
            this.expButton.MouseLeave += new System.EventHandler(this.expButton_MouseLeave);
            // 
            // divButton
            // 
            this.divButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.divButton.Location = new System.Drawing.Point(129, 19);
            this.divButton.Name = "divButton";
            this.divButton.Size = new System.Drawing.Size(122, 50);
            this.divButton.TabIndex = 1;
            this.divButton.Text = "x/y";
            this.divButton.UseVisualStyleBackColor = true;
            this.divButton.Click += new System.EventHandler(this.divButton_Click);
            this.divButton.MouseEnter += new System.EventHandler(this.divButton_MouseEnter);
            this.divButton.MouseLeave += new System.EventHandler(this.divButton_MouseLeave);
            // 
            // addButton
            // 
            this.addButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addButton.Location = new System.Drawing.Point(252, 19);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(122, 50);
            this.addButton.TabIndex = 2;
            this.addButton.Text = "x&+y";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            this.addButton.MouseEnter += new System.EventHandler(this.addButton_MouseEnter);
            this.addButton.MouseLeave += new System.EventHandler(this.addButton_MouseLeave);
            // 
            // minusButton
            // 
            this.minusButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minusButton.Location = new System.Drawing.Point(252, 74);
            this.minusButton.Name = "minusButton";
            this.minusButton.Size = new System.Drawing.Size(122, 50);
            this.minusButton.TabIndex = 5;
            this.minusButton.Text = "x&-y";
            this.minusButton.UseVisualStyleBackColor = true;
            this.minusButton.Click += new System.EventHandler(this.minusButton_Click);
            this.minusButton.MouseEnter += new System.EventHandler(this.minusButton_MouseEnter);
            this.minusButton.MouseLeave += new System.EventHandler(this.minusButton_MouseLeave);
            // 
            // oneDivXButton
            // 
            this.oneDivXButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.oneDivXButton.Location = new System.Drawing.Point(129, 74);
            this.oneDivXButton.Name = "oneDivXButton";
            this.oneDivXButton.Size = new System.Drawing.Size(122, 50);
            this.oneDivXButton.TabIndex = 4;
            this.oneDivXButton.Text = "1&/x";
            this.oneDivXButton.UseVisualStyleBackColor = true;
            this.oneDivXButton.Click += new System.EventHandler(this.oneDivXButton_Click);
            this.oneDivXButton.MouseEnter += new System.EventHandler(this.oneDivXButton_MouseEnter);
            this.oneDivXButton.MouseLeave += new System.EventHandler(this.oneDivXButton_MouseLeave);
            // 
            // factButton
            // 
            this.factButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.factButton.Location = new System.Drawing.Point(6, 74);
            this.factButton.Name = "factButton";
            this.factButton.Size = new System.Drawing.Size(122, 50);
            this.factButton.TabIndex = 3;
            this.factButton.Text = "x&!";
            this.factButton.UseVisualStyleBackColor = true;
            this.factButton.Click += new System.EventHandler(this.factButton_Click);
            this.factButton.MouseEnter += new System.EventHandler(this.factButton_MouseEnter);
            this.factButton.MouseLeave += new System.EventHandler(this.factButton_MouseLeave);
            // 
            // multTableButton
            // 
            this.multTableButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.multTableButton.Location = new System.Drawing.Point(252, 128);
            this.multTableButton.Name = "multTableButton";
            this.multTableButton.Size = new System.Drawing.Size(122, 50);
            this.multTableButton.TabIndex = 8;
            this.multTableButton.Text = "Multiplication &Tablle of x";
            this.multTableButton.UseVisualStyleBackColor = true;
            this.multTableButton.Click += new System.EventHandler(this.multTableButton_Click);
            this.multTableButton.MouseEnter += new System.EventHandler(this.multTableButton_MouseEnter);
            this.multTableButton.MouseLeave += new System.EventHandler(this.multTableButton_MouseLeave);
            // 
            // stDivButton
            // 
            this.stDivButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stDivButton.Location = new System.Drawing.Point(129, 128);
            this.stDivButton.Name = "stDivButton";
            this.stDivButton.Size = new System.Drawing.Size(122, 50);
            this.stDivButton.TabIndex = 7;
            this.stDivButton.Text = "Sta&ndard Deviation of x,y";
            this.stDivButton.UseVisualStyleBackColor = true;
            this.stDivButton.Click += new System.EventHandler(this.stDivButton_Click);
            this.stDivButton.MouseEnter += new System.EventHandler(this.stDivButton_MouseEnter);
            this.stDivButton.MouseLeave += new System.EventHandler(this.stDivButton_MouseLeave);
            // 
            // primeButton
            // 
            this.primeButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.primeButton.Location = new System.Drawing.Point(6, 128);
            this.primeButton.Name = "primeButton";
            this.primeButton.Size = new System.Drawing.Size(122, 50);
            this.primeButton.TabIndex = 6;
            this.primeButton.Text = "&Prime";
            this.primeButton.UseVisualStyleBackColor = true;
            this.primeButton.Click += new System.EventHandler(this.primeButton_Click);
            this.primeButton.MouseEnter += new System.EventHandler(this.primeButton_MouseEnter);
            this.primeButton.MouseLeave += new System.EventHandler(this.primeButton_MouseLeave);
            // 
            // sumValuesButton
            // 
            this.sumValuesButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sumValuesButton.Location = new System.Drawing.Point(252, 184);
            this.sumValuesButton.Name = "sumValuesButton";
            this.sumValuesButton.Size = new System.Drawing.Size(122, 50);
            this.sumValuesButton.TabIndex = 11;
            this.sumValuesButton.Text = "Sum of F&ile Values";
            this.sumValuesButton.UseVisualStyleBackColor = true;
            this.sumValuesButton.Click += new System.EventHandler(this.sumValuesButton_Click);
            this.sumValuesButton.MouseEnter += new System.EventHandler(this.sumValuesButton_MouseEnter);
            this.sumValuesButton.MouseLeave += new System.EventHandler(this.sumValuesButton_MouseLeave);
            // 
            // maxButton
            // 
            this.maxButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxButton.Location = new System.Drawing.Point(129, 184);
            this.maxButton.Name = "maxButton";
            this.maxButton.Size = new System.Drawing.Size(122, 50);
            this.maxButton.TabIndex = 10;
            this.maxButton.Text = "Max of &File Values";
            this.maxButton.UseVisualStyleBackColor = true;
            this.maxButton.Click += new System.EventHandler(this.maxButton_Click);
            this.maxButton.MouseEnter += new System.EventHandler(this.maxButton_MouseEnter);
            this.maxButton.MouseLeave += new System.EventHandler(this.maxButton_MouseLeave);
            // 
            // sosButton
            // 
            this.sosButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sosButton.Location = new System.Drawing.Point(6, 184);
            this.sosButton.Name = "sosButton";
            this.sosButton.Size = new System.Drawing.Size(122, 50);
            this.sosButton.TabIndex = 9;
            this.sosButton.Text = "&Sum of Squares";
            this.sosButton.UseVisualStyleBackColor = true;
            this.sosButton.Click += new System.EventHandler(this.sosButton_Click);
            this.sosButton.MouseEnter += new System.EventHandler(this.sosButton_MouseEnter);
            this.sosButton.MouseLeave += new System.EventHandler(this.sosButton_MouseLeave);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.yeetCheckBox);
            this.panel1.Controls.Add(this.clearEntryButton);
            this.panel1.Controls.Add(this.zeroButton);
            this.panel1.Controls.Add(this.lilClearButton);
            this.panel1.Controls.Add(this.threeButton);
            this.panel1.Controls.Add(this.twoButton);
            this.panel1.Controls.Add(this.oneButton);
            this.panel1.Controls.Add(this.sixButton);
            this.panel1.Controls.Add(this.fiveButton);
            this.panel1.Controls.Add(this.fourButton);
            this.panel1.Controls.Add(this.nineButton);
            this.panel1.Controls.Add(this.eightButton);
            this.panel1.Controls.Add(this.sevenButton);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.exitButton);
            this.panel1.Controls.Add(this.BigClearButton);
            this.panel1.Controls.Add(this.displayButton);
            this.panel1.Controls.Add(this.yTextBox);
            this.panel1.Controls.Add(this.xTextBox);
            this.panel1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(381, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(199, 574);
            this.panel1.TabIndex = 1;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(9, 5);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(174, 95);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // yeetCheckBox
            // 
            this.yeetCheckBox.AutoSize = true;
            this.yeetCheckBox.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yeetCheckBox.Location = new System.Drawing.Point(22, 344);
            this.yeetCheckBox.Name = "yeetCheckBox";
            this.yeetCheckBox.Size = new System.Drawing.Size(15, 14);
            this.yeetCheckBox.TabIndex = 13;
            this.yeetCheckBox.UseVisualStyleBackColor = true;
            this.yeetCheckBox.CheckedChanged += new System.EventHandler(this.yeetCheckBox_CheckedChanged);
            this.yeetCheckBox.MouseEnter += new System.EventHandler(this.yeetCheckBox_MouseEnter);
            this.yeetCheckBox.MouseLeave += new System.EventHandler(this.yeetCheckBox_MouseLeave);
            // 
            // clearEntryButton
            // 
            this.clearEntryButton.BackColor = System.Drawing.Color.Maroon;
            this.clearEntryButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearEntryButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.clearEntryButton.Location = new System.Drawing.Point(127, 230);
            this.clearEntryButton.Name = "clearEntryButton";
            this.clearEntryButton.Size = new System.Drawing.Size(45, 34);
            this.clearEntryButton.TabIndex = 11;
            this.clearEntryButton.Text = "C&E";
            this.clearEntryButton.UseVisualStyleBackColor = false;
            this.clearEntryButton.Click += new System.EventHandler(this.clearEntryButton_Click);
            this.clearEntryButton.MouseEnter += new System.EventHandler(this.clearEntryButton_MouseEnter);
            this.clearEntryButton.MouseLeave += new System.EventHandler(this.clearEntryButton_MouseLeave);
            // 
            // zeroButton
            // 
            this.zeroButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zeroButton.Location = new System.Drawing.Point(78, 230);
            this.zeroButton.Name = "zeroButton";
            this.zeroButton.Size = new System.Drawing.Size(45, 34);
            this.zeroButton.TabIndex = 10;
            this.zeroButton.Text = "&0";
            this.zeroButton.UseVisualStyleBackColor = true;
            this.zeroButton.Click += new System.EventHandler(this.zeroButton_Click);
            this.zeroButton.MouseEnter += new System.EventHandler(this.zeroButton_MouseEnter);
            this.zeroButton.MouseLeave += new System.EventHandler(this.zeroButton_MouseLeave);
            // 
            // lilClearButton
            // 
            this.lilClearButton.BackColor = System.Drawing.Color.Maroon;
            this.lilClearButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lilClearButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lilClearButton.Location = new System.Drawing.Point(28, 230);
            this.lilClearButton.Name = "lilClearButton";
            this.lilClearButton.Size = new System.Drawing.Size(45, 34);
            this.lilClearButton.TabIndex = 9;
            this.lilClearButton.Text = "&C";
            this.lilClearButton.UseVisualStyleBackColor = false;
            this.lilClearButton.Click += new System.EventHandler(this.lilClearButton_Click);
            this.lilClearButton.MouseEnter += new System.EventHandler(this.lilClearButton_MouseEnter);
            this.lilClearButton.MouseLeave += new System.EventHandler(this.lilClearButton_MouseLeave);
            // 
            // threeButton
            // 
            this.threeButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeButton.Location = new System.Drawing.Point(127, 190);
            this.threeButton.Name = "threeButton";
            this.threeButton.Size = new System.Drawing.Size(45, 34);
            this.threeButton.TabIndex = 8;
            this.threeButton.Text = "&3";
            this.threeButton.UseVisualStyleBackColor = true;
            this.threeButton.Click += new System.EventHandler(this.threeButton_Click);
            this.threeButton.MouseEnter += new System.EventHandler(this.threeButton_MouseEnter);
            this.threeButton.MouseLeave += new System.EventHandler(this.threeButton_MouseLeave);
            // 
            // twoButton
            // 
            this.twoButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.twoButton.Location = new System.Drawing.Point(78, 190);
            this.twoButton.Name = "twoButton";
            this.twoButton.Size = new System.Drawing.Size(45, 34);
            this.twoButton.TabIndex = 7;
            this.twoButton.Text = "&2";
            this.twoButton.UseVisualStyleBackColor = true;
            this.twoButton.Click += new System.EventHandler(this.twoButton_Click);
            this.twoButton.MouseEnter += new System.EventHandler(this.twoButton_MouseEnter);
            this.twoButton.MouseLeave += new System.EventHandler(this.twoButton_MouseLeave);
            // 
            // oneButton
            // 
            this.oneButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.oneButton.Location = new System.Drawing.Point(28, 190);
            this.oneButton.Name = "oneButton";
            this.oneButton.Size = new System.Drawing.Size(45, 34);
            this.oneButton.TabIndex = 6;
            this.oneButton.Text = "&1";
            this.oneButton.UseVisualStyleBackColor = true;
            this.oneButton.Click += new System.EventHandler(this.oneButton_Click);
            this.oneButton.MouseEnter += new System.EventHandler(this.oneButton_MouseEnter);
            this.oneButton.MouseLeave += new System.EventHandler(this.oneButton_MouseLeave);
            // 
            // sixButton
            // 
            this.sixButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sixButton.Location = new System.Drawing.Point(127, 150);
            this.sixButton.Name = "sixButton";
            this.sixButton.Size = new System.Drawing.Size(45, 34);
            this.sixButton.TabIndex = 5;
            this.sixButton.Text = "&6";
            this.sixButton.UseVisualStyleBackColor = true;
            this.sixButton.Click += new System.EventHandler(this.sixButton_Click);
            this.sixButton.MouseEnter += new System.EventHandler(this.sixButton_MouseEnter);
            this.sixButton.MouseLeave += new System.EventHandler(this.sixButton_MouseLeave);
            // 
            // fiveButton
            // 
            this.fiveButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fiveButton.Location = new System.Drawing.Point(78, 150);
            this.fiveButton.Name = "fiveButton";
            this.fiveButton.Size = new System.Drawing.Size(45, 34);
            this.fiveButton.TabIndex = 4;
            this.fiveButton.Text = "&5";
            this.fiveButton.UseVisualStyleBackColor = true;
            this.fiveButton.Click += new System.EventHandler(this.fiveButton_Click);
            this.fiveButton.MouseEnter += new System.EventHandler(this.fiveButton_MouseEnter);
            this.fiveButton.MouseLeave += new System.EventHandler(this.fiveButton_MouseLeave);
            // 
            // fourButton
            // 
            this.fourButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fourButton.Location = new System.Drawing.Point(28, 150);
            this.fourButton.Name = "fourButton";
            this.fourButton.Size = new System.Drawing.Size(45, 34);
            this.fourButton.TabIndex = 3;
            this.fourButton.Text = "&4";
            this.fourButton.UseVisualStyleBackColor = true;
            this.fourButton.Click += new System.EventHandler(this.fourButton_Click);
            this.fourButton.MouseEnter += new System.EventHandler(this.fourButton_MouseEnter);
            this.fourButton.MouseLeave += new System.EventHandler(this.fourButton_MouseLeave);
            // 
            // nineButton
            // 
            this.nineButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nineButton.Location = new System.Drawing.Point(125, 110);
            this.nineButton.Name = "nineButton";
            this.nineButton.Size = new System.Drawing.Size(45, 34);
            this.nineButton.TabIndex = 2;
            this.nineButton.Text = "&9";
            this.nineButton.UseVisualStyleBackColor = true;
            this.nineButton.Click += new System.EventHandler(this.nineButton_Click);
            this.nineButton.MouseEnter += new System.EventHandler(this.nineButton_MouseEnter);
            this.nineButton.MouseLeave += new System.EventHandler(this.nineButton_MouseLeave);
            // 
            // eightButton
            // 
            this.eightButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eightButton.Location = new System.Drawing.Point(76, 110);
            this.eightButton.Name = "eightButton";
            this.eightButton.Size = new System.Drawing.Size(45, 34);
            this.eightButton.TabIndex = 1;
            this.eightButton.Text = "&8";
            this.eightButton.UseVisualStyleBackColor = true;
            this.eightButton.Click += new System.EventHandler(this.eightButton_Click);
            this.eightButton.MouseEnter += new System.EventHandler(this.eightButton_MouseEnter);
            this.eightButton.MouseLeave += new System.EventHandler(this.eightButton_MouseLeave);
            // 
            // sevenButton
            // 
            this.sevenButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sevenButton.Location = new System.Drawing.Point(26, 110);
            this.sevenButton.Name = "sevenButton";
            this.sevenButton.Size = new System.Drawing.Size(45, 34);
            this.sevenButton.TabIndex = 0;
            this.sevenButton.Text = "&7";
            this.sevenButton.UseVisualStyleBackColor = true;
            this.sevenButton.Click += new System.EventHandler(this.sevenButton_Click);
            this.sevenButton.MouseEnter += new System.EventHandler(this.sevenButton_MouseEnter);
            this.sevenButton.MouseLeave += new System.EventHandler(this.sevenButton_MouseLeave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(43, 344);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 15);
            this.label3.TabIndex = 17;
            this.label3.Text = "y:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(43, 318);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "x:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // exitButton
            // 
            this.exitButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.exitButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitButton.Location = new System.Drawing.Point(50, 499);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(94, 41);
            this.exitButton.TabIndex = 17;
            this.exitButton.Text = "E&xit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            this.exitButton.MouseEnter += new System.EventHandler(this.exitButton_MouseEnter);
            this.exitButton.MouseLeave += new System.EventHandler(this.exitButton_MouseLeave);
            // 
            // BigClearButton
            // 
            this.BigClearButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BigClearButton.Location = new System.Drawing.Point(50, 452);
            this.BigClearButton.Name = "BigClearButton";
            this.BigClearButton.Size = new System.Drawing.Size(94, 41);
            this.BigClearButton.TabIndex = 16;
            this.BigClearButton.Text = "C&lear Results";
            this.BigClearButton.UseVisualStyleBackColor = true;
            this.BigClearButton.Click += new System.EventHandler(this.BigClearButton_Click);
            this.BigClearButton.MouseEnter += new System.EventHandler(this.BigClearButton_MouseEnter);
            this.BigClearButton.MouseLeave += new System.EventHandler(this.BigClearButton_MouseLeave);
            // 
            // displayButton
            // 
            this.displayButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displayButton.Location = new System.Drawing.Point(50, 405);
            this.displayButton.Name = "displayButton";
            this.displayButton.Size = new System.Drawing.Size(94, 41);
            this.displayButton.TabIndex = 15;
            this.displayButton.Text = "&Display File";
            this.displayButton.UseVisualStyleBackColor = true;
            this.displayButton.Click += new System.EventHandler(this.displayButton_Click);
            this.displayButton.MouseEnter += new System.EventHandler(this.displayButton_MouseEnter);
            this.displayButton.MouseLeave += new System.EventHandler(this.displayButton_MouseLeave);
            // 
            // yTextBox
            // 
            this.yTextBox.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yTextBox.Location = new System.Drawing.Point(68, 341);
            this.yTextBox.Name = "yTextBox";
            this.yTextBox.Size = new System.Drawing.Size(100, 23);
            this.yTextBox.TabIndex = 14;
            this.yTextBox.TextChanged += new System.EventHandler(this.yTextBox_TextChanged);
            this.yTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.yTextBox_KeyPress);
            // 
            // xTextBox
            // 
            this.xTextBox.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xTextBox.Location = new System.Drawing.Point(68, 315);
            this.xTextBox.Name = "xTextBox";
            this.xTextBox.Size = new System.Drawing.Size(100, 23);
            this.xTextBox.TabIndex = 12;
            this.xTextBox.TextChanged += new System.EventHandler(this.xTextBox_TextChanged);
            this.xTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.xTextBox_KeyPress);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LightGray;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(0, 1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(383, 70);
            this.panel2.TabIndex = 0;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.LightGray;
            this.panel3.Controls.Add(this.valueAtButton);
            this.panel3.Controls.Add(this.avgButton);
            this.panel3.Controls.Add(this.rangeButton);
            this.panel3.Controls.Add(this.expButton);
            this.panel3.Controls.Add(this.divButton);
            this.panel3.Controls.Add(this.addButton);
            this.panel3.Controls.Add(this.sumValuesButton);
            this.panel3.Controls.Add(this.factButton);
            this.panel3.Controls.Add(this.maxButton);
            this.panel3.Controls.Add(this.oneDivXButton);
            this.panel3.Controls.Add(this.sosButton);
            this.panel3.Controls.Add(this.minusButton);
            this.panel3.Controls.Add(this.multTableButton);
            this.panel3.Controls.Add(this.primeButton);
            this.panel3.Controls.Add(this.stDivButton);
            this.panel3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(0, 252);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(383, 310);
            this.panel3.TabIndex = 0;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // valueAtButton
            // 
            this.valueAtButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueAtButton.Location = new System.Drawing.Point(252, 242);
            this.valueAtButton.Name = "valueAtButton";
            this.valueAtButton.Size = new System.Drawing.Size(122, 50);
            this.valueAtButton.TabIndex = 14;
            this.valueAtButton.Text = "File Array &Value at (x,y)";
            this.valueAtButton.UseVisualStyleBackColor = true;
            this.valueAtButton.Click += new System.EventHandler(this.valueAtButton_Click);
            this.valueAtButton.MouseEnter += new System.EventHandler(this.valueAtButton_MouseEnter);
            this.valueAtButton.MouseLeave += new System.EventHandler(this.valueAtButton_MouseLeave);
            // 
            // avgButton
            // 
            this.avgButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgButton.Location = new System.Drawing.Point(129, 242);
            this.avgButton.Name = "avgButton";
            this.avgButton.Size = new System.Drawing.Size(122, 50);
            this.avgButton.TabIndex = 13;
            this.avgButton.Text = "&Average of File Values";
            this.avgButton.UseVisualStyleBackColor = true;
            this.avgButton.Click += new System.EventHandler(this.avgButton_Click);
            this.avgButton.MouseEnter += new System.EventHandler(this.avgButton_MouseEnter);
            this.avgButton.MouseLeave += new System.EventHandler(this.avgButton_MouseLeave);
            // 
            // rangeButton
            // 
            this.rangeButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rangeButton.Location = new System.Drawing.Point(6, 242);
            this.rangeButton.Name = "rangeButton";
            this.rangeButton.Size = new System.Drawing.Size(122, 50);
            this.rangeButton.TabIndex = 12;
            this.rangeButton.Text = "&Range of File Values";
            this.rangeButton.UseVisualStyleBackColor = true;
            this.rangeButton.Click += new System.EventHandler(this.rangeButton_Click);
            this.rangeButton.MouseEnter += new System.EventHandler(this.rangeButton_MouseEnter);
            this.rangeButton.MouseLeave += new System.EventHandler(this.rangeButton_MouseLeave);
            // 
            // MainForm
            // 
            this.AcceptButton = this.displayButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.exitButton;
            this.ClientSize = new System.Drawing.Size(581, 552);
            this.Controls.Add(this.outputListBox);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(597, 608);
            this.MinimumSize = new System.Drawing.Size(597, 558);
            this.Name = "MainForm";
            this.Text = "Calculator Application";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox outputListBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button expButton;
        private System.Windows.Forms.Button divButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button minusButton;
        private System.Windows.Forms.Button oneDivXButton;
        private System.Windows.Forms.Button factButton;
        private System.Windows.Forms.Button multTableButton;
        private System.Windows.Forms.Button stDivButton;
        private System.Windows.Forms.Button primeButton;
        private System.Windows.Forms.Button sumValuesButton;
        private System.Windows.Forms.Button maxButton;
        private System.Windows.Forms.Button sosButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Button BigClearButton;
        private System.Windows.Forms.Button displayButton;
        private System.Windows.Forms.TextBox yTextBox;
        private System.Windows.Forms.TextBox xTextBox;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button clearEntryButton;
        private System.Windows.Forms.Button zeroButton;
        private System.Windows.Forms.Button lilClearButton;
        private System.Windows.Forms.Button threeButton;
        private System.Windows.Forms.Button twoButton;
        private System.Windows.Forms.Button oneButton;
        private System.Windows.Forms.Button sixButton;
        private System.Windows.Forms.Button fiveButton;
        private System.Windows.Forms.Button fourButton;
        private System.Windows.Forms.Button nineButton;
        private System.Windows.Forms.Button eightButton;
        private System.Windows.Forms.Button sevenButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button valueAtButton;
        private System.Windows.Forms.Button avgButton;
        private System.Windows.Forms.Button rangeButton;
        private System.Windows.Forms.CheckBox yeetCheckBox;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

