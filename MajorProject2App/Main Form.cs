﻿//Developer Names: Noah Bimrose and Zack Clade
//Application Name: Special Functions Calculator
//Application Purpose: To take user input and perform an assortment of basic, and more advancd calculations 
//Date: 11/6/2018

//Division of project work: Noah did most of the coding of the button functions (driver) and Zack did the UI and served as a logical consult (navigator). 
//Due to unavoidable hardships and loss of a family member, Zack was not able to do as much work in the end which would have been ideal. However, when able he did make the effort to contribute. 
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace MajorProject2App
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            xTextBox.Select();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            // Exit Confirmation
            DialogResult dialog = MessageBox.Show("Are you sure you want to exit?", this.Text,
            MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            //If answer is yes, close the app
            if (dialog == DialogResult.Yes)
                this.Close();
        }

        private void BigClearButton_Click(object sender, EventArgs e)
        {
            //clears the listbox of all data
            outputListBox.Items.Clear();
        }

        private void oneButton_Click(object sender, EventArgs e)
        {
            //If statement checks the state of the y checkbox selection to direct user input to the corrct control
            if (yeetCheckBox.Checked == false)
            {
                //appends the additional digit after the existing input value
                xTextBox.Text += "1";
            }
            else if (yeetCheckBox.Checked == true)
            {
                //appends the additional digit after the existing input value
                yTextBox.Text += "1";

            }

        }

        private void zeroButton_Click(object sender, EventArgs e)
        {
            //If statement checks the state of the y checkbox selection to direct user input to the corrct control
            if (yeetCheckBox.Checked == false)
            {
                //appends the additional digit after the existing input value
                xTextBox.Text += "0";
            }
            else if (yeetCheckBox.Checked == true)
            {
                //appends the additional digit after the existing input value
                yTextBox.Text += "0";

            }
        }

        private void twoButton_Click(object sender, EventArgs e)
        {
            //If statement checks the state of the y checkbox selection to direct user input to the corrct control
            if (yeetCheckBox.Checked == false)
            {
                //appends the additional digit after the existing input value
                xTextBox.Text += "2";
            }
            else if (yeetCheckBox.Checked == true)
            {
                //appends the additional digit after the existing input value
                yTextBox.Text += "2";

            }
        }

        private void threeButton_Click(object sender, EventArgs e)
        {
            //If statement checks the state of the y checkbox selection to direct user input to the corrct control
            if (yeetCheckBox.Checked == false)
            {
                //appends the additional digit after the existing input value
                xTextBox.Text += "3";
            }
            else if (yeetCheckBox.Checked == true)
            {
                //appends the additional digit after the existing input value
                yTextBox.Text += "3";

            }
        }

        private void fourButton_Click(object sender, EventArgs e)
        {
            //If statement checks the state of the y checkbox selection to direct user input to the corrct control
            if (yeetCheckBox.Checked == false)
            {
                //appends the additional digit after the existing input value
                xTextBox.Text += "4";
            }
            else if (yeetCheckBox.Checked == true)
            {
                //appends the additional digit after the existing input value
                yTextBox.Text += "4";

            }
        }

        private void fiveButton_Click(object sender, EventArgs e)
        {
            //If statement checks the state of the y checkbox selection to direct user input to the corrct control
            if (yeetCheckBox.Checked == false)
            {
                //appends the additional digit after the existing input value
                xTextBox.Text += "5";
            }
            else if (yeetCheckBox.Checked == true)
            {
                //appends the additional digit after the existing input value
                yTextBox.Text += "5";

            }
        }

        private void sixButton_Click(object sender, EventArgs e)
        {
            //If statement checks the state of the y checkbox selection to direct user input to the corrct control
            if (yeetCheckBox.Checked == false)
            {
                //appends the additional digit after the existing input value
                xTextBox.Text += "6";
            }
            else if (yeetCheckBox.Checked == true)
            {
                //parses the inout each time the user clicks the button and assigns it to a varialbe
                double.TryParse(yTextBox.Text, out double keypadY);
                //appends the additional digit after the existing input value
                yTextBox.Text += "6";

            }
        }

        private void sevenButton_Click(object sender, EventArgs e)
        {
            //If statement checks the state of the y checkbox selection to direct user input to the corrct control
            if (yeetCheckBox.Checked == false)
            {
                //appends the additional digit after the existing input value
                xTextBox.Text += "7";
            }
            else if (yeetCheckBox.Checked == true)
            {
                //appends the additional digit after the existing input value
                yTextBox.Text += "7";

            }
        }

        private void eightButton_Click(object sender, EventArgs e)
        {
            //If statement checks the state of the y checkbox selection to direct user input to the corrct control
            if (yeetCheckBox.Checked == false)
            {
                //appends the additional digit after the existing input value
                xTextBox.Text += "8";
            }
            else if (yeetCheckBox.Checked == true)
            {

                //appends the additional digit after the existing input value
                yTextBox.Text += "8";

            }
        }

        private void nineButton_Click(object sender, EventArgs e)
        {
            //If statement checks the state of the y checkbox selection to direct user input to the corrct control
            if (yeetCheckBox.Checked == false)
            {
                //appends the additional digit after the existing input value
                xTextBox.Text += "9";
            }
            else if (yeetCheckBox.Checked == true)
            {
                //appends the additional digit after the existing input value
                yTextBox.Text += "9";

            }
        }

        private void lilClearButton_Click(object sender, EventArgs e)
        {
            //If statement checks the state of the y checkbox selection to direct the clearing to the corect control
            if (yeetCheckBox.Checked == false)
            {
                //clears ajust the x input values
                xTextBox.Text = "";
            }
            else if (yeetCheckBox.Checked == true)
            {
                //clears just the y input value

                yTextBox.Text = "";

            }
        }

        private void clearEntryButton_Click(object sender, EventArgs e)
        {
            try
            {
                //If statement checks the state of the y checkbox selection to direct the clearing to the corect control
                if (yeetCheckBox.Checked == false)
                {
                    //clears ajust the x input values
                    string entryMinusOneX = xTextBox.Text.Substring(0, xTextBox.Text.Length - 1);//deletes just the last charater of the input, code from assignment pdf
                    xTextBox.Text = entryMinusOneX;
                }
                else if (yeetCheckBox.Checked == true)
                {
                    //clears just the y input value
                    string entryMinusOneY = yTextBox.Text.Substring(0, yTextBox.Text.Length - 1);//deletes just the last charater of the input, code from assignment pdf
                    yTextBox.Text = entryMinusOneY;
                }
            }

            catch
            {
                //notifies user that they need to put in more numbers if they want to keep clearing stuff
                MessageBox.Show("Nothing to clear, please enter a value.", "Field Empty");
            }
        }

        private void factButton_Click(object sender, EventArgs e)
        {
            outputListBox.Items.Clear();// clears listbox before outputing
            //gets user input, and declares variables 
            int.TryParse(xTextBox.Text, out int x);

            double fact = 1;
            if (string.IsNullOrWhiteSpace(xTextBox.Text))//checks for a blank input and notifies user
            {
                MessageBox.Show("Please enter an x value .", "Error");
                xTextBox.Focus();
            }
            else if (x <= 0)
            {
                MessageBox.Show("Please enter a number greater than zero.", "Error");
                xTextBox.Focus();
            }

            else
            {
                //loop that performs the math for a factorial of the keyed number
                for (int i = 1; i <= x; i++)
                {
                    fact *= i;
                }
                //Writes out the factorial equation to the list box
                outputListBox.Items.Add(x.ToString() + "!:" + fact.ToString());
            }


        }

        private void expButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(xTextBox.Text))//checks for a blank input and notifies user
            {
                MessageBox.Show("Please enter an x value .", "Error");
                xTextBox.Focus();
            }
            else if (string.IsNullOrWhiteSpace(yTextBox.Text))//checks for a blank input and notifies user
            {
                MessageBox.Show("Please enter a y value.", "Error");
                yTextBox.Focus();
            }
            else
            {
                outputListBox.Items.Clear();// clears listbox before outputing
                                            //gets user input for x and y and assigns the to variables
                int.TryParse(xTextBox.Text, out int x);
                int.TryParse(yTextBox.Text, out int y);
                //declares a variable and set it equal the the exponent equation
                double expResult = Math.Pow(x, y);
                //Writes out the complete equation to the list box
                outputListBox.Items.Add(x.ToString() + " raised to the power of " + y.ToString() + ": " + expResult.ToString("N0"));
            }

        }

        private void divButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(xTextBox.Text))//checks for a blank input and notifies user
            {
                MessageBox.Show("Please enter an x value .", "Error");
                xTextBox.Focus();
            }
            else if (string.IsNullOrWhiteSpace(yTextBox.Text))//checks for a blank input and notifies user
            {
                MessageBox.Show("Please enter a y value.", "Error");
                yTextBox.Focus();
            }
            else
            {
                outputListBox.Items.Clear();// clears listbox before outputing
                                            //gets user input for x and y and assigns the to variables
                int.TryParse(xTextBox.Text, out int x);
                int.TryParse(yTextBox.Text, out int y);
                //declares a variable and set it equal the the division equation
                double divResult = x / y;
                //Writes out the complete equation to the list box
                outputListBox.Items.Add(x.ToString() + "/" + y.ToString() + " = " + divResult.ToString());
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(xTextBox.Text))//checks for a blank input and notifies user
            {
                MessageBox.Show("Please enter an x value .", "Error");
                xTextBox.Focus();
            }
            else if (string.IsNullOrWhiteSpace(yTextBox.Text))//checks for a blank input and notifies user
            {
                MessageBox.Show("Please enter a y value.", "Error");
                yTextBox.Focus();
            }
            else
            {
                outputListBox.Items.Clear();// clears listbox before outputing
                                            //gets user input for x and y and assigns the to variables
                int.TryParse(xTextBox.Text, out int x);
                int.TryParse(yTextBox.Text, out int y);
                //declares a variable and set it equal the the addition equation
                double addResult = x + y;
                //Writes out the complete equation to the list box
                outputListBox.Items.Add(x.ToString() + "+" + y.ToString() + " = " + addResult.ToString());
            }
        }

        private void minusButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(xTextBox.Text))//checks for a blank input and notifies user
            {
                MessageBox.Show("Please enter an x value .", "Error");
                xTextBox.Focus();
            }
            else if (string.IsNullOrWhiteSpace(yTextBox.Text))//checks for a blank input and notifies user
            {
                MessageBox.Show("Please enter a y value.", "Error");
                yTextBox.Focus();
            }
            else
            {
                outputListBox.Items.Clear();// clears listbox before outputing
                                            //Code taken from midterm application and adapted

                //gets user input for x and y and assigns the to variables
                int.TryParse(xTextBox.Text, out int x);
                int.TryParse(yTextBox.Text, out int y);

                //declares a variable and set it equal the the subtraction equation
                double minusResult = x - y;

                //Writes out the complete equation to the list box
                outputListBox.Items.Add("Difference of " + x.ToString() + " and " + y.ToString() + ": " + minusResult.ToString());
            }
        }

        private void multTableButton_Click(object sender, EventArgs e)
        {

            outputListBox.Items.Clear();// clears listbox of prior output
            if (string.IsNullOrWhiteSpace(xTextBox.Text))//checks for a blank input and notifies user
            {
                MessageBox.Show("Please enter an x value .", "Error");
                xTextBox.Focus();
            }
            else
            {
                // Parse the number as an integer out of the text box and declare variables
                int.TryParse(xTextBox.Text, out int number);//parses inthe users input
                int product;//holder for the final calculation that is read into the listbox


                // Check if the user inputted a value outside the bounds of 1 and 12 inclusive, if they did then display a message and return from the function
                if (number < 1 || number > 12)
                {
                    MessageBox.Show("Please enter a number between 1 and 12");//message directs user to put correct parameters
                    xTextBox.SelectAll();//selects all the control text
                    xTextBox.Focus();//sets the focus to the offending control input
                    return;
                }

                // If the number is within [1,12], loop through the products and display the calculation, while storing the aggregate in the total values
                for (int count = 1; count <= 12; count++)//calculates and prints each line of the multiplication table 
                {
                    product = count * number;//calculates the product of the current loop values
                    outputListBox.Items.Add(number.ToString("N0") + "*" + count.ToString("N0") + "=" + product.ToString("N0"));//prints out the text of the muliplication table
                }
            }
        }

        private void oneDivXButton_Click(object sender, EventArgs e)
        {
            outputListBox.Items.Clear();// clears listbox before outputing
            //gets user input for x and assigns to a variable
            int.TryParse(xTextBox.Text, out int x);

            if (x == 0)
            {
                MessageBox.Show("Can't divide by zero. Please try again.", "Error");
            }
            else
            {
                //Writes out the complete equation to the list box
                outputListBox.Items.Add("Reciprocal of " + x.ToString() + " : " + "1/" + x.ToString());
            }

        }

        private void displayButton_Click(object sender, EventArgs e)
        {
            string currentLine;//declares a holding string for the document read in

            StreamReader NumbersReader = new StreamReader("numbers.txt");//opens a new reader to allow the file to be read in the program

            outputListBox.Items.Clear();// clears listbox before outputing

            while (NumbersReader.EndOfStream == false)//loops through the lines in the file until there are no more charaters to read

            {
                currentLine = NumbersReader.ReadLine();//reads a line off the file and sets it to a holding string
                outputListBox.Items.Add(currentLine);//prints the current line value to the listbox
            }
            NumbersReader.Close();//closes the document reader
        }

        private void maxButton_Click(object sender, EventArgs e)
        {
            outputListBox.Items.Clear();// clears listbox before outputing
            //Declare 1-D array
            string[] numbers = new string[30];//final array used to perform math operation 
            string[] fields;//temp array used as holder during splitting

            int numindex = 0, fieldsIndex = 0;//initializes index variable used for incrementing
            string currentLine;//holder variable the will actually be the thing being split 
            StreamReader NumbersReader = new StreamReader("numbers.txt");//creates a new streamreader so the csv can be read in the program

            while (NumbersReader.EndOfStream == false)//runs until the reader runs out of charaters to read
            {
                currentLine = NumbersReader.ReadLine(); // reads in the file one line at a time and assigns it to a holding string variable


                while (fieldsIndex <= 4)//steps through each element of the line until the whole line has been assigned to a new array element
                {
                    fields = currentLine.Split(','); // split current line and store the returned array of stings in a holding array
                    numbers[numindex] = fields[fieldsIndex]; //transfers the values of the holding array into a new element in the final array
                    fieldsIndex += 1;//increments holding array index by one so the next element can be transffered
                    numindex += 1;// increments the final array by one so it can accept the next holder array element
                }
                fieldsIndex = 0;//sets the holding array index back to 0 so a new line can be read and then reassigned
            }

            string maxVal = numbers.Max();//gets aray max 

            outputListBox.Items.Clear();//clears listbox before outputting
            outputListBox.Items.Add("Max file number is:" + maxVal);//outputs the max array value to the listbox

            NumbersReader.Close();//closes the document reader
        }

        private void sumValuesButton_Click(object sender, EventArgs e)
        {
            outputListBox.Items.Clear();// clears listbox before outputing
            double sum = 0;//will hold final output for displaying
            //Declare 1-D arrays
            string[] numbers = new string[30];//final array used to perform math operation 
            string[] fields;//temp array used as holder during splitting

            int numindex = 0, fieldsIndex = 0;//initializes index variable used for incrementing
            string currentLine;//holder variable the will actually be the thing being split 
            StreamReader NumbersReader = new StreamReader("numbers.txt");//creates a new streamreader so the csv can be read in the program

            while (NumbersReader.EndOfStream == false)//runs until the reader runs out of charaters to read
            {
                currentLine = NumbersReader.ReadLine(); // reads in the file one line at a time and assigns it to a holding string variable


                while (fieldsIndex <= 4)//steps through each element of the line until the whole line has been assigned to a new array element
                {
                    fields = currentLine.Split(','); // split current line and store the returned array of stings in a holding array
                    numbers[numindex] = fields[fieldsIndex]; //transfers the values of the holding array into a new element in the final array
                    fieldsIndex += 1;//increments holding array index by one so the next element can be transffered
                    numindex += 1;// increments the final array by one so it can accept the next holder array element
                }
                fieldsIndex = 0;//sets the holding array index back to 0 so a new line can be read and then reassigned
            }

            outputListBox.Items.Clear();//clears listbox before outputing 

            for (int i = 0; i < numbers.Length; i++)//loops thorugh all the elements in the array so each value can be added up
            {
                sum += (double.Parse(numbers[i]));//casts the array string value to a double so it can be added to the running total

            }
            outputListBox.Items.Add("Sum of file numbers is:" + sum);//outputs the file total to the listbox
            NumbersReader.Close();//closes the document reader
        }

        private void rangeButton_Click(object sender, EventArgs e)
        {

            //Declare 1-D array
            string[] numbers = new string[30];//final array used to perform math operation 
            string[] fields;//temp array used as holder during splitting

            int numindex = 0, fieldsIndex = 0;//initializes index variable used for incrementing
            string currentLine;//holder variable the will actually be the thing being split 
            StreamReader NumbersReader = new StreamReader("numbers.txt");//creates a new streamreader so the csv can be read in the program

            while (NumbersReader.EndOfStream == false)//runs until the reader runs out of charaters to read
            {
                currentLine = NumbersReader.ReadLine(); // reads in the file one line at a time and assigns it to a holding string variable


                while (fieldsIndex <= 4)//steps through each element of the line until the whole line has been assigned to a new array element
                {
                    fields = currentLine.Split(','); // split current line and store the returned array of stings in a holding array
                    numbers[numindex] = fields[fieldsIndex]; //transfers the values of the holding array into a new element in the final array
                    fieldsIndex += 1;//increments holding array index by one so the next element can be transffered
                    numindex += 1;// increments the final array by one so it can accept the next holder array element
                }
                fieldsIndex = 0;//sets the holding array index back to 0 so a new line can be read and then reassigned
            }



            string maxVal = numbers.Max();//finds aray max 
            string minVal = numbers.Min(); //finds array min
            double.TryParse(maxVal, out double max);//casts the max to a double 
            double.TryParse(minVal, out double min);//casts the min to a double
            double range = max - min;//calculates the range
            outputListBox.Items.Clear();//clears listbox before outputting
            outputListBox.Items.Add("Range of file values:" + range.ToString());//ouputs te range into the listbox
            NumbersReader.Close();//closes the document reader

        }

        private void primeButton_Click(object sender, EventArgs e)
        {
            //Based on posts on this Source:https://stackoverflow.com/questions/16583665/for-loop-to-calculate-factorials

            outputListBox.Items.Clear();// clears listbox before outputing

            //initializes variables and gets user input
            double primeNum;
            int.TryParse(xTextBox.Text, out int x);
            int.TryParse(yTextBox.Text, out int y);

            //checks inputs for invalid entries below 
            if (x == 0 || x < 0)
            {
                MessageBox.Show("Enter an x value greater than 0", "Error");
                xTextBox.Focus();
            }
            else if (x > y)
            {
                MessageBox.Show("Enter a y value larger than the x entry", "Error");
            }
            else if (y > 500)
            {
                MessageBox.Show("Please Enter a value no greater than 500 for y.", "Error");
            }
            else//if the inout is valid, it starts checking conditions to determine prime numbers
            {
                for (primeNum = x; primeNum <= y; primeNum++)//loops through all numbers in the range and looks at these conditions
                {
                    //manually sets the first 8 primes so the next statment won't incorectly mark them
                    if ((primeNum == 2) | (primeNum == 3) | (primeNum == 5) | (primeNum == 7) | (primeNum == 11) | (primeNum == 13) | (primeNum == 17) | (primeNum == 19))
                    {
                        outputListBox.Items.Add(primeNum.ToString() + " is a prime number");//outputs the number and displays it as prime 
                    }
                    //checks to see if any number has a remainder once divided by the 1st 8 prime numbers and marks them as not prime. This is vailid from 1-500; a larger range would require more conditions
                    else if ((primeNum % 2 == 0) | (primeNum % 3 == 0) | (primeNum % 5 == 0) | (primeNum % 7 == 0) | (primeNum % 11 == 0) | (primeNum % 13 == 0) | (primeNum % 17 == 0) | (primeNum % 19 == 0))
                    {
                        outputListBox.Items.Add(primeNum.ToString() + " is not a prime number");//outputs the number and displays it as not prime 

                    }
                    else
                    //if the number is aything else, it is prime
                    {
                        outputListBox.Items.Add(primeNum.ToString() + " is a prime number");//outputs the number and displays it as prime 
                    }
                }
            }
        }

        private void valueAtButton_Click(object sender, EventArgs e)
        {
            //code based on example in textbook for using 2D arrays with documents
            outputListBox.Items.Clear();// clears listbox before outputing
            //initializes indexes, arrays, and placholder variables for reading in documents
            string[,] numbers = new string[6, 5];
            int recordCount;
            string currentLine;
            string[] fields = new string[2];
            int row = 0, col = 0;
            //open a stream reader
            StreamReader numbersReader = new StreamReader("numbers.txt");

            //Read and split each line of the file and put in 2D array
            while (numbersReader.EndOfStream == false)
            {
                currentLine = numbersReader.ReadLine();//Reads row and col of data
                fields = currentLine.Split(',');//stores data in temp array

                //copies row and col to the cols of the current row
                for (col = 0; col <= 4; col++)
                {
                    numbers[row, col] = fields[col];//take the feilds elements and assigns them to the correct row in numbers
                }

                row++;//increments the row index so the next row can be placed into numbers
            }
            recordCount = row;

            // casts strings to ints for math operation
            int.TryParse(xTextBox.Text, out int x);
            int.TryParse(yTextBox.Text, out int y);


            //checks to see if the user entry is outside the file bound and issues error or blank; set focus to offending inout
            if (string.IsNullOrWhiteSpace(xTextBox.Text))//checks for a blank input and notifies user
            {
                MessageBox.Show("Please enter an x value .", "Error");
                xTextBox.Focus();
            }
            else if (string.IsNullOrWhiteSpace(yTextBox.Text))//checks for a blank input and notifies user
            {
                MessageBox.Show("Please enter a y value.", "Error");
                yTextBox.Focus();
            }
            else if (x > 5 || x < 0)//checks x inout bound
            {
                MessageBox.Show("X entry is outside the range of the data. Please try again.", "Input Not Valid");
                xTextBox.Focus();
            }
            else if (y > 4 || y < 0)//checks y inout bound
            {
                MessageBox.Show("Y entry is outside the range of the data. Please try again.", "Input Not Valid");
                yTextBox.Focus();
            }
            else//allows display of element
            {
                outputListBox.Items.Add("Value is:" + numbers[x, y]);//displays the selected element
            }
        }

        private void avgButton_Click(object sender, EventArgs e)
        {
            float sum = 0, avg = 0;//used to hold math operations conducted on string data types

            //Declare 1-D array
            string[] numbers = new string[30];//final array used to perform math operation 
            string[] fields;//temp array used as holder during splitting

            int numindex = 0, fieldsIndex = 0;//initializes index variable used for incrementing
            string currentLine;//holder variable the will actually be the thing being split 
            StreamReader NumbersReader = new StreamReader("numbers.txt");//creates a new streamreader so the csv can be read in the program

            while (NumbersReader.EndOfStream == false)//runs until the reader runs out of charaters to read
            {
                currentLine = NumbersReader.ReadLine(); // reads in the file one line at a time and assigns it to a holding string variable


                while (fieldsIndex <= 4)//steps through each element of the line until the whole line has been assigned to a new array element
                {
                    fields = currentLine.Split(','); // split current line and store the returned array of stings in a holding array
                    numbers[numindex] = fields[fieldsIndex]; //transfers the values of the holding array into a new element in the final array
                    fieldsIndex += 1;//increments holding array index by one so the next element can be transffered
                    numindex += 1;// increments the final array by one so it can accept the next holder array element
                }
                fieldsIndex = 0;//sets the holding array index back to 0 so a new line can be read and then reassigned
            }

            outputListBox.Items.Clear();//clears the listbox before outputting

            for (int i = 0; i < numbers.Length; i++)//loop steps through each array element inorder to add the values in each to the sum variable
            {
                sum += (float.Parse(numbers[i]));//parses the string array values to a float to be able to compute the sum

            }

            avg = sum / numbers.Length;//calculates the average off all the values in the number array

            outputListBox.Items.Add("Average values:" + avg.ToString("N2"));//prints the average to the listbox
            NumbersReader.Close();//closes the documet reader
        }

        private void sosButton_Click(object sender, EventArgs e)
        {
            outputListBox.Items.Clear();// clears listbox before outputing
            //Parses in user inot and assigns them to variables
            int.TryParse(xTextBox.Text, out int x);
            int.TryParse(yTextBox.Text, out int y);

            if (x == 0)//checks to see if no input was made in the x input
            {
                MessageBox.Show("Enter a value larger than 0 for x", "Error");//tells user to enter a value within set parameters
                xTextBox.Focus();//sends them to the incorrect input control

            }
            else if (y == 0)//checks to see if no input was made in the y input
            {
                MessageBox.Show("Enter a value larger than 0 for y", "Error");//tells user to enter a value within set parameters
                yTextBox.Focus();//sends them to the incorrect input control
            }

            else if (x > y)
            {
                MessageBox.Show("Enter a y value larger than the x entry", "Error");//tells user to enter a value within set parameters
                yTextBox.Focus();//sends them to the incorrect input control
            }
            else
            {
                //declares varibles for claculations
                double sumNum = 0, sqrAns = 0, sumSqu = 0;

                do//loop performs calculations on each value and keeps a running total for displaying
                {
                    sqrAns = Math.Pow(x, 2);//squares the x number
                    sumNum += x;//adds the x to a variable to track the sum
                    sumSqu += sqrAns;//adds up all the squared x values
                    outputListBox.Items.Add("The number is: " + x + " and its square is " + sqrAns);//outputs the info for each value to a line in the lst box
                    x ++; //increments the number to run again
                }

                while (x <= y);//out outputs a space and the final sum off all numbers and their squares once the range of values has been incremented through
                {
                    outputListBox.Items.Add(" ");
                    outputListBox.Items.Add("Sum of numbers: " + sumNum + "  Sum of squares: " + sumSqu);
                }
            }

        }
        private void stDivButton_Click(object sender, EventArgs e)
        {
            outputListBox.Items.Clear();// clears listbox before outputing
            
            //Declares needed varibles for calculations
            double sqrDiff = 0, sum = 0, sumSqu = 0, stdDev = 0, count, mean = 0,  var = 0, xCount, diff = 0 ;

            int xSwitchHolder = 0;//holder var for swapping 

            //Gets  user input
            int.TryParse(xTextBox.Text, out int x);
            int.TryParse(yTextBox.Text, out int y);

            if (string.IsNullOrWhiteSpace(xTextBox.Text))//checks for a blank input and notifies user
            {
                MessageBox.Show("Please enter an x value .", "Error");
                xTextBox.Focus();
            }
            else if (string.IsNullOrWhiteSpace(yTextBox.Text))//checks for a blank input and notifies user
            {
                MessageBox.Show("Please enter a y value.", "Error");
                yTextBox.Focus();
            }
            else if (x == 0)//disallows zero as an x input
            {
                MessageBox.Show("Please enter an x value greater than zero.", "Error");
                xTextBox.Focus();
            }

            else if (y == 0)//disallows zero as a y input 
            {
                MessageBox.Show("Please enter a y value greater than zero.", "Error");
                yTextBox.Focus();
            }
            else if (x > y)//checks to see if the x input is greater than the y to see if they need to be flipped
            {
                xSwitchHolder = x;//assigns the x to a holding var 
                x = y;//overwrites the x value with the y value
                y = xSwitchHolder; // places the x value into the y value so the calcualtion can be ran as intended

                count = (y - x) + 1; //count 

                for (xCount = x; xCount <= y; xCount++)
                {
                    sum += xCount; //sum of all numbers added
                }

                mean = sum / count; //finds the mean

                for (xCount = x; xCount <= y; xCount++)
                {
                    diff = xCount - mean; //differences
                    sqrDiff = Math.Pow(diff, 2); //square differnces
                    sumSqu += sqrDiff; //sum of square differences 
                }
                var = (sumSqu / (count - 1)); //vairance 
                stdDev = Math.Sqrt(var); //standard deviation
                outputListBox.Items.Add("Standard deviation for range " + x + " to " + y + " : " + stdDev.ToString("N6"));

            }
            else//normal calculation if the input swapping is not needed
            {
                count = (y - x) + 1; //count 

                for (xCount = x; xCount <= y; xCount++)
                {
                    sum += xCount; //sum of all numbers added
                }

                mean = sum / count; //finds the mean

                for (xCount = x; xCount <= y; xCount++)
                {
                    diff = xCount - mean; //differences
                    sqrDiff = Math.Pow(diff, 2); //square differnces
                    sumSqu += sqrDiff; //sum of square differences 
                }
                var = (sumSqu / (count - 1)); //vairance 
                stdDev = Math.Sqrt(var); //standard deviation
                outputListBox.Items.Add("Standard deviation for range " + x + " to " + y + " : " + stdDev.ToString("N6"));
            }

        }

        private void xTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            //This handler only allows digit, control characters, and the period.

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.' && e.KeyChar != '-')
            {
                e.Handled = true;
                return;
            }
        }

        private void yTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            //This handler only allows digit, control characters, and the period.

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.' && e.KeyChar != '-')
            {
                e.Handled = true;
                return;
            }
        }



        //The following lines change the color of each button, as named, to LightSteelBlue when the mouse hovers over them.
        //Every clickable button has this feature and is listed below, in the same format.

        private void addButton_MouseEnter(object sender, EventArgs e)
        {
            addButton.BackColor = Color.LightSteelBlue;
        }

        private void divButton_MouseEnter(object sender, EventArgs e)
        {
            divButton.BackColor = Color.LightSteelBlue;
        }

        private void expButton_MouseEnter(object sender, EventArgs e)
        {
            expButton.BackColor = Color.LightSteelBlue;
        }

        private void factButton_MouseEnter(object sender, EventArgs e)
        {
            factButton.BackColor = Color.LightSteelBlue;
        }

        private void oneDivXButton_MouseEnter(object sender, EventArgs e)
        {
            oneDivXButton.BackColor = Color.LightSteelBlue;
        }

        private void minusButton_MouseEnter(object sender, EventArgs e)
        {
            minusButton.BackColor = Color.LightSteelBlue;
        }

        private void multTableButton_MouseEnter(object sender, EventArgs e)
        {
            multTableButton.BackColor = Color.LightSteelBlue;
        }

        private void stDivButton_MouseEnter(object sender, EventArgs e)
        {
            stDivButton.BackColor = Color.LightSteelBlue;
        }

        private void primeButton_MouseEnter(object sender, EventArgs e)
        {
            primeButton.BackColor = Color.LightSteelBlue;
        }

        private void sosButton_MouseEnter(object sender, EventArgs e)
        {
            sosButton.BackColor = Color.LightSteelBlue;
        }

        private void maxButton_MouseEnter(object sender, EventArgs e)
        {
            maxButton.BackColor = Color.LightSteelBlue;
        }

        private void sumValuesButton_MouseEnter(object sender, EventArgs e)
        {
            sumValuesButton.BackColor = Color.LightSteelBlue;
        }

        private void valueAtButton_MouseEnter(object sender, EventArgs e)
        {
            valueAtButton.BackColor = Color.LightSteelBlue;
        }

        private void avgButton_MouseEnter(object sender, EventArgs e)
        {
            avgButton.BackColor = Color.LightSteelBlue;
        }

        private void rangeButton_MouseEnter(object sender, EventArgs e)
        {
            rangeButton.BackColor = Color.LightSteelBlue;
        }

        private void displayButton_MouseEnter(object sender, EventArgs e)
        {
            displayButton.BackColor = Color.LightSteelBlue;
        }

        private void BigClearButton_MouseEnter(object sender, EventArgs e)
        {
            BigClearButton.BackColor = Color.LightSteelBlue;
        }

        private void exitButton_MouseEnter(object sender, EventArgs e)
        {
            exitButton.BackColor = Color.LightSteelBlue;
        }

        private void sevenButton_MouseEnter(object sender, EventArgs e)
        {
            sevenButton.BackColor = Color.LightSteelBlue;
        }

        private void eightButton_MouseEnter(object sender, EventArgs e)
        {
            eightButton.BackColor = Color.LightSteelBlue;
        }

        private void nineButton_MouseEnter(object sender, EventArgs e)
        {
            nineButton.BackColor = Color.LightSteelBlue;
        }

        private void fourButton_MouseEnter(object sender, EventArgs e)
        {
            fourButton.BackColor = Color.LightSteelBlue;
        }

        private void fiveButton_MouseEnter(object sender, EventArgs e)
        {
            fiveButton.BackColor = Color.LightSteelBlue;
        }

        private void sixButton_MouseEnter(object sender, EventArgs e)
        {
            sixButton.BackColor = Color.LightSteelBlue;
        }

        private void threeButton_MouseEnter(object sender, EventArgs e)
        {
            threeButton.BackColor = Color.LightSteelBlue;
        }

        private void zeroButton_MouseEnter(object sender, EventArgs e)
        {
            zeroButton.BackColor = Color.LightSteelBlue;
        }

        private void clearEntryButton_MouseEnter(object sender, EventArgs e)
        {
            clearEntryButton.BackColor = Color.LightSteelBlue;
        }

        private void lilClearButton_MouseEnter(object sender, EventArgs e)
        {
            lilClearButton.BackColor = Color.LightSteelBlue;
        }

        private void oneButton_MouseEnter(object sender, EventArgs e)
        {
            oneButton.BackColor = Color.LightSteelBlue;
        }

        private void twoButton_MouseEnter(object sender, EventArgs e)
        {
            twoButton.BackColor = Color.LightSteelBlue;
        }

        private void yeetCheckBox_MouseEnter(object sender, EventArgs e)
        {
            yeetCheckBox.BackColor = Color.LightSteelBlue;
        }

        //The following lines change the color of each button, as named, from LightSteelBlue back to LightGrey, when the mouse
        //leaves the button
        //Every clickable button has this feature and is listed below, in the same format.

        private void expButton_MouseLeave(object sender, EventArgs e)
        {
            expButton.BackColor = Color.LightGray;
        }

        private void factButton_MouseLeave(object sender, EventArgs e)
        {
            factButton.BackColor = Color.LightGray;
        }

        private void primeButton_MouseLeave(object sender, EventArgs e)
        {
            primeButton.BackColor = Color.LightGray;
        }

        private void sosButton_MouseLeave(object sender, EventArgs e)
        {
            sosButton.BackColor = Color.LightGray;
        }

        private void rangeButton_MouseLeave(object sender, EventArgs e)
        {
            rangeButton.BackColor = Color.LightGray;
        }

        private void divButton_MouseLeave(object sender, EventArgs e)
        {
            divButton.BackColor = Color.LightGray;
        }

        private void oneDivXButton_MouseLeave(object sender, EventArgs e)
        {
            oneDivXButton.BackColor = Color.LightGray;
        }

        private void stDivButton_MouseLeave(object sender, EventArgs e)
        {
            stDivButton.BackColor = Color.LightGray;
        }

        private void maxButton_MouseLeave(object sender, EventArgs e)
        {
            maxButton.BackColor = Color.LightGray;
        }

        private void avgButton_MouseLeave(object sender, EventArgs e)
        {
            avgButton.BackColor = Color.LightGray;
        }

        private void addButton_MouseLeave(object sender, EventArgs e)
        {
            addButton.BackColor = Color.LightGray;
        }

        private void minusButton_MouseLeave(object sender, EventArgs e)
        {
            minusButton.BackColor = Color.LightGray;
        }

        private void multTableButton_MouseLeave(object sender, EventArgs e)
        {
            multTableButton.BackColor = Color.LightGray;
        }

        private void sumValuesButton_MouseLeave(object sender, EventArgs e)
        {
            sumValuesButton.BackColor = Color.LightGray;
        }

        private void valueAtButton_MouseLeave(object sender, EventArgs e)
        {
            valueAtButton.BackColor = Color.LightGray;
        }

        private void sevenButton_MouseLeave(object sender, EventArgs e)
        {
            sevenButton.BackColor = Color.LightGray;
        }

        private void fourButton_MouseLeave(object sender, EventArgs e)
        {
            fourButton.BackColor = Color.LightGray;
        }

        private void oneButton_MouseLeave(object sender, EventArgs e)
        {
            oneButton.BackColor = Color.LightGray;
        }

        private void lilClearButton_MouseLeave(object sender, EventArgs e)
        {
            lilClearButton.BackColor = Color.Maroon;
        }

        private void eightButton_MouseLeave(object sender, EventArgs e)
        {
            eightButton.BackColor = Color.LightGray;
        }

        private void fiveButton_MouseLeave(object sender, EventArgs e)
        {
            fiveButton.BackColor = Color.LightGray;
        }

        private void twoButton_MouseLeave(object sender, EventArgs e)
        {
            twoButton.BackColor = Color.LightGray;
        }

        private void zeroButton_MouseLeave(object sender, EventArgs e)
        {
            zeroButton.BackColor = Color.LightGray;
        }

        private void nineButton_MouseLeave(object sender, EventArgs e)
        {
            nineButton.BackColor = Color.LightGray;
        }

        private void sixButton_MouseLeave(object sender, EventArgs e)
        {
            sixButton.BackColor = Color.LightGray;
        }

        private void threeButton_MouseLeave(object sender, EventArgs e)
        {
            threeButton.BackColor = Color.LightGray;
        }

        private void clearEntryButton_MouseLeave(object sender, EventArgs e)
        {
            clearEntryButton.BackColor = Color.Maroon;
        }

        private void yeetCheckBox_MouseLeave(object sender, EventArgs e)
        {
            yeetCheckBox.BackColor = Color.LightGray;
        }

        private void displayButton_MouseLeave(object sender, EventArgs e)
        {
            displayButton.BackColor = Color.LightGray;
        }

        private void BigClearButton_MouseLeave(object sender, EventArgs e)
        {
            BigClearButton.BackColor = Color.LightGray;
        }

        private void exitButton_MouseLeave(object sender, EventArgs e)
        {
            exitButton.BackColor = Color.LightGray;
        }

        private void outputListBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void yeetCheckBox_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void yTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void xTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }
    }
}
/*
 *                 for (col = 0; col <= 4; col++)
                {
                    numbers[row, col] = fields[col];
                }
*/


